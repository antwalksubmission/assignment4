package Bank;

import Accounts.Account;
import Accounts.CurrentAccount;
import Accounts.SavingsAccount;
import Customer.Customer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Bank {
   static ArrayList<Customer> CustomerList = new ArrayList<>();
   static HashMap<String, Customer> CusDetails = new HashMap<>();
   static ArrayList<SavingsAccount> SavingsAccountList = new ArrayList<>();
   static ArrayList<CurrentAccount> CurrentAccountList = new ArrayList<>();
   static Scanner sc = new Scanner(System.in);

    static void createAccount()
    {
        System.out.println("****Enter the account holder's details****");
        System.out.println("If exisisting Customer press 1 else press 0");
        Customer owner;
        Account ac ;
        String[] cusdetails;
        int input = Integer.parseInt(sc.nextLine());
        if(input==1)
        {
            System.out.println("Enter Username and Password separated by ;");
            cusdetails = sc.nextLine().split(";");
            owner=CustomerLogin(cusdetails[0],cusdetails[1]);
        }
        else
        {
            System.out.println("Enter Customer details : Name, SSN, phoneNumber, age separated by ;");
            cusdetails =sc.nextLine().split(";");
            owner = createCustomer(cusdetails[0],cusdetails[1],cusdetails[2],Integer.parseInt(cusdetails[3]));
            while(!owner.validateAge())
            {
                System.out.println("Enter age greater than 18 years");
                owner.setAge(Integer.parseInt(sc.nextLine()));
            }

            while (!owner.validateSSn())
            {
                System.out.println("Enter SSN of 9 digits");
                owner.setSSN(sc.nextLine());
            }
        }
        if(owner.UserName!=null)
        {
            System.out.println("Enter type of account to be created: press 1 for current, 0 for savings");
            input = Integer.parseInt(sc.nextLine());
            if(input==1)
            {
                if(owner.CurrentAccountCount==0) {
                    ac = new CurrentAccount(owner);
                    owner.setCurrentAccount((CurrentAccount) ac);
                    CurrentAccountList.add((CurrentAccount) ac);
                    System.out.println("Account created successfully.");
                    ac.display();
                }
                else {
                    System.out.println("Customer already has a current account");
                }
            }
            else if(input==0){
                if(owner.SavingsAccountCount==0) {
                    ac = new SavingsAccount(owner);
                    owner.setSavingsAccount((SavingsAccount) ac);
                    SavingsAccountList.add((SavingsAccount) ac);
                    System.out.println("Account created successfully.");
                    ac.display();
                }
                else {
                    System.out.println("Customer already has a savings account");
                }
            }
        }
    }

    static Customer createCustomer(String Name, String SSN, String phoneNumber,int age)
    {
        Long phone =Long.parseLong(phoneNumber);
        Customer c1 = new Customer(Name,SSN,phone,age);
        CustomerList.add(c1);
        CusDetails.put(c1.UserName,c1);
        System.out.println("Customer created successfully. The details are: \n" + c1);
        return c1;
    }

    static Customer CustomerLogin(String username, String password)
    {
        if(CusDetails.containsKey(username))
        {
            boolean safe = CusDetails.get(username).login(password);
            if (safe)
            {
                System.out.println("Logged in successfully");
                return CusDetails.get(username);
            }
            else {
                System.out.println("Password is wrong");
            }
        }
        else {
            System.out.println("Wrong Username");
        }
        return new Customer();
    }

    static void Bankapp()
    {
        while (true)
        {
            System.out.println("*****************Welcome to ABC Bank Private Ltd.************************");
            System.out.println("Enter the UserName");
            Scanner sc = new Scanner(System.in);
            String uname = sc.nextLine();
            System.out.println("Enter the passWord");
            String pword = sc.nextLine();
            Customer c=CustomerLogin(uname,pword);
            if(c.UserName==null)
            {
                System.out.println("Sorry Can't proceed anymore");
            }
            else
            {
                while(true)
                {
                    System.out.println("Please Enter Your Choice\n1.Deposit\n2.Withdrawal\n3.CheckBalance\n4.Edit Profile\n5.Change Password\n6.Exit");
                    int choice = Integer.parseInt(sc.nextLine());
                    switch (choice) {
                        case (1) -> {
                            System.out.println("Enter the amount of money to deposit");
                            int amount = Integer.parseInt(sc.nextLine());
                            c.deposit(amount);
                        }
                        case (2) -> {
                            System.out.println("Enter the amount of money to withdraw");
                            int amount = Integer.parseInt(sc.nextLine());
                            c.withdrawal(amount);
                        }
                        case (3) -> c.checkBalance();
                        case (4) -> c.editProfile();
                        case (5) -> c.changePassword();
                    }
                    if(choice==6)
                        break;
                }
            }
            System.out.println("Press "+"YES"+" to continue and "+"NO"+" to end");
            String choice = sc.nextLine();
            if(choice.equals("NO"))
                break;
        }

    }

    public static void main(String[] args) {
        createAccount();
        createAccount();
        Bankapp();
    }

}
