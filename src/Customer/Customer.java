package Customer;

import Accounts.CurrentAccount;

import java.util.Scanner;

public class Customer implements Validation{
    static int count=0;
    String Name;
    public String UserName;
    private String Password;
    int Age;
    String SSN;
    //fixed 9 digits
    String Address;
    String Email;
    Long Phone;
    float Balance;
    String CusId;
    Accounts.SavingsAccount SavingsAccount;
    CurrentAccount CurrentAccount;
    public int SavingsAccountCount=0;
    public int CurrentAccountCount=0;

    public void setAge(int age) {
        Age = age;
    }

    public void setSSN(String SSN) {
        this.SSN = SSN;
    }

    public String getName() {
        return Name;
    }

    public Accounts.SavingsAccount getSavingsAccount() {
        return SavingsAccount;
    }

    public Accounts.CurrentAccount getCurrentAccount() {
        return CurrentAccount;
    }


    public void setSavingsAccount(Accounts.SavingsAccount savingsAccount) {
        this.SavingsAccount = savingsAccount;
        SavingsAccountCount=1;
    }

    public void setCurrentAccount(CurrentAccount currentAccount) {
        this.CurrentAccount = currentAccount;
        CurrentAccountCount=1;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "Name='" + Name + '\'' +
                ", UserName='" + UserName + '\'' +
                ", Password='" + Password + '\'' +
                ", Age=" + Age +
                ", SSN='" + SSN + '\'' +
                ", Address='" + Address + '\'' +
                ", Email='" + Email + '\'' +
                ", Phone=" + Phone +
                ", Balance=" + Balance +
                ", CusId='" + CusId + '\'' +
                '}';
    }

    public Customer()
    {
        System.out.println("This is for use of a random customer");
    }
    public Customer(String name, String ssn, Long phone, int age) {
        this.Name=name;
        this.SSN=ssn;
        this.Phone=phone;
        this.UserName = Name.split(" ")[0] + String.valueOf(phone).substring(6, 10);
        this.Password = SSN.substring(5,9);
        this.Balance=0.0f;
        this.Age=age;
        count++;
    }

    public boolean login(String password)
    {
        return this.Password.equals(password);
    }

    public void deposit(float amount)
    {
        this.Balance+=amount;
    }

    public void withdrawal(float amount)
    {
        if(this.Balance<=amount)
            System.out.println("This transaction can't proceed due to insufficient account balance");
        else
            this.Balance-=amount;
    }

    public void checkBalance()
    {
        System.out.println("Your account balance is "+this.Balance);
    }

    public void editProfile()
    {
        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.println("Please enter your choice");
            System.out.println("To change UserName press - 2\nTo change Address press - 3\nTo change Email press - 4\nTo change phone press - 5");
            int choice = Integer.parseInt(sc.nextLine());
            switch (choice) {
                case (2) -> {
                    System.out.println("Enter new Age");
                    this.Age = Integer.parseInt(sc.nextLine());
                }
                case (3) -> {
                    System.out.println("Enter new Address");
                    this.Address = sc.nextLine();
                }
                case (4) -> {
                    System.out.println("Enter new Email");
                    this.Email = sc.nextLine();
                }
                case (5) -> {
                    System.out.println("Enter new Phone");
//                    String phone;
//                    phone = sc.nextLine();
                    this.Phone=Long.parseLong(sc.nextLine());
//                    this.Phone = Long.parseLong(phone);
                }
                default -> System.out.println("Enter some valid number");
            }
            System.out.println("To continue press 1 and to end press 0");
            String condition = sc.nextLine();
            if(condition.equals("0"))
                break;
        }

        System.out.println("New Chnged details are "+this);

    }

    public void changePassword()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the old Password");
        String oldPassword = sc.nextLine();
        if(this.Password.equals(oldPassword))
        {
            System.out.println("Enter the new Password");
            String newPassword = sc.nextLine();
            System.out.println("Again enter the new password to confirm");
            if(newPassword.equals(sc.nextLine()))
            {
                this.Password=newPassword;
            }

        }
        System.out.println("Password Changed Successfully");
    }


    public boolean validateSSn() {
        if(this.SSN.length()==9)
            return true;
        else {
            System.out.println("SSN must be of 9 digits");
            return false;
        }
    }

    @Override
    public boolean validateAge() {
        if(this.Age>=18)
            return true;
        else {
            System.out.println("The age must be greater than 18");
            return false;
        }
    }
}
