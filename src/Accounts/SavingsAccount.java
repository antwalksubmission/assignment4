package Accounts;

import Customer.Customer;

public class SavingsAccount extends Account{
    public static int count=1;
    public SavingsAccount(Customer owner) {
        super("BC"+count,"Savings", 0.0f,owner);
        count++;

    }

    @Override
    public String toString() {
        return "SavingsAccount{" +
                "accountId='" + accountId + '\'' +
                ", description='" + description + '\'' +
                ", minimumBalance=" + minimumBalance +
                '}';
    }

    @Override
    public void display()
    {
        System.out.println("The details are "+this);
    }
}
