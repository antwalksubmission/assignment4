package Accounts;

import Customer.Customer;



public abstract class Account {
    String accountId;
    String description;
    Float minimumBalance;
    Customer owner;



    public Account(String accountId, String description, Float minimumBalance, Customer owner) {
        this.accountId = accountId;
        this.description = description;
        this.minimumBalance = minimumBalance;
        this.owner=owner;
    }

    public String getAccountId() {
        return accountId;
    }

        abstract public void display();

}
